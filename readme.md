<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>


## About this demo project

This project is made upon request from [Square1](http://square1.io), it is developed using Laravel 5.4.15 and its best practices.  It does three basic tasks: 
- Retrieve top 10 most expensive & top 10 cheapest products from [appliancedelivered.io](http://appliancedelivered.io)
- Add and remove products to your wish list.
- Update your profile (name and email).

## Installation

To install, clone the repository to a local folder making sure the 
appropriate permissions are set for `/storage` and `/bootstrap/cache`. Then configure your database credentials within your `.env` file, you can use any supported cache driver, we recomend `database`:
```php
CACHE_DRIVER=database
```
Next step is to run the migrations:
```php
php artisan migrate
```

Last step, install all dependencies with composer:
```
composer install
```
## Testing your installation
To test that everything is installed and configured correctly, you can run the included tests using:
```
php artisan dusk
```
##Usage
The project home page will show two separated lists of the top ten most expensive and top ten cheapest products from [appliancedelivered.io](http://appliancedelivered.io), 
after you register and log in, you will be able to save products to your wish list.
Products are saved to the database only when a user adds them to their wish list.

## About Laravel
The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
