<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Square1 Test</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        {{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">--}}
        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

        </style>
    </head>
    <body  class="flex-center position-ref full-height">
        <div>
            @if (Route::has('login'))
                <div class="top-right links">
                    @if (Auth::check())
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ url('/login') }}">Login</a>
                        <a href="{{ url('/register') }}">Register</a>
                    @endif
                </div>
            @endif

            <div class="content">
                <h1>Square1 Test</h1>
                <p>This is a demo project for <a href="http://square1.io">Square1</a>
                    made by <strong><a href="mailto:jmarchan@gmail.com">Johan Marchán</a></strong>.
                Please <a href="{{ url('/register') }}">Register</a> and <a href="{{ url('/login') }}">log in</a> to add products to your Wish List</p>

                <div class="row">
                    <div class="col-md-6">
                        @foreach($expensive as $name => $price)
                            @if($loop->first)
                                <h2>Most Expensive</h2>

                                <table class="table table-condensed">
                            @endif
                            <tr>
                                <td class="text-left">{{ $name }}</td>
                                <td>{{ $price }}</td>
                            </tr>

                            @if($loop->last)
                                </table>
                            @endif
                        @endforeach
                    </div>

                    <div class="col-md-6">
                        @foreach($cheap as $name => $price)
                            @if($loop->first)
                                <h2>Cheapest</h2>
                                <table class="table table-condensed">
                            @endif
                                    <tr>
                                        <td class="text-left">{{ $name }}</td>
                                        <td>{{ $price }}</td>
                                    </tr>

                            @if($loop->last)
                                </table>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
