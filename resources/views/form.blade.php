{{ Form::open(['url' => '/home']) }}
<div class="row">
    <div class="col-md-6">
        @foreach($expensive as $name => $price)
            @if($loop->first)
                <h2>Most Expensive</h2>

                <table class="table table-condensed">
                    @endif
                    <tr>
                        <td>{{ Form::checkbox($name, $price) }}</td>
                        <td class="text-left">{{ $name }}</td>
                        <td>{{ $price }}</td>
                    </tr>

                    @if($loop->last)
                </table>
            @endif
        @endforeach
    </div>

    <div class="col-md-6">
        @foreach($cheap as $name => $price)
            @if($loop->first)
                <h2>Cheapest</h2>
                <table class="table table-condensed">
            @endif
                <tr>
                    <td>{{ Form::checkbox($name, $price) }}</td>
                    <td class="text-left">{{ $name }}</td>
                    <td>{{ $price }}</td>
                </tr>
            @if($loop->last)
                </table>
            @endif
        @endforeach
    </div>
</div>

<div class="row">

    <div class="text-center">{{ Form::submit('Add to wishlist', ['class' => 'btn btn-default']) }}</div>
</div>
{{ Form::close() }}
