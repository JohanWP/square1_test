@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Your Wish List</div>

                    <div class="panel-body">
                        @if($products->count() < 1)
                            <p>Your wish list is empty.  You can add some products from the <a href="/home">Home page</a></p>
                        @else

                        {!! Form::open([
                            'method'=>'POST',
                            'action'=>['HomeController@removeFromWishList'],
                            'class' => 'form-horizontal'
                            ]) !!}
                        @foreach($products as $product)
                            @if ($loop->first)
                                <table class="table table-condensed">
                                    <thead>
                                        <tr>
                                            <th>&nbsp;</th>
                                            <th>Product Name</th>
                                            <th>Price</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                            @endif

                            <tr>
                                <td>{!! Form::checkbox('p_'.$product->id, $product->id) !!}</td>
                                <td>{{ $product->name }}</td>
                                <td>€{{ $product->price }}</td>
                            </tr>

                            @if ($loop->last)
                                    </tbody>
                                </table>
                            @endif
                        @endforeach
                        <a href="/home" class="btn btn-default">Cancel</a>
                        {!! Form::submit('Remove selected items', ['class' => 'btn btn-danger']) !!}
                        {!! Form::close() !!}
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection