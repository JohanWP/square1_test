{{ Form::open() }}

<div class="row">
    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
        <label for="name" class="col-md-4 control-label">Name</label>

        <div class="col-md-6">
            {{--<input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>--}}
            {!! Form::text('name', null, ['class'=>'form-control', 'required' => 'required']) !!}
            @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
        <label for="email" class="col-md-4 control-label">E-Mail Address</label>

        <div class="col-md-6">
            {{--<input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>--}}
            {!! Form::text('email', null, ['class'=>'form-control', 'required' => 'required']) !!}
            @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
        </div>
    </div>

</div>
{{--<div class="row">
    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
        <label for="password" class="col-md-4 control-label">Password</label>

        <div class="col-md-6">
            --}}{{--<input id="password" type="password" class="form-control" name="password" required>--}}{{--
            {!! Form::password('password', ['class'=>'form-control']) !!}
            @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="form-group">
        <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

        <div class="col-md-6">
            --}}{{--<input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>--}}{{--
            {!! Form::password('password_confirmation', ['class'=>'form-control']) !!}

        </div>
    </div>
</div>--}}
   <div class="form-group">
        <div class="col-md-6 col-md-offset-4">
            <a href="javascript:window.history.back()" class="btn btn-default">Cancel</a>
            {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
        </div>
    </div>

</form>
