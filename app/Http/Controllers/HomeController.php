<?php

namespace App\Http\Controllers;

use App\Http\Traits\GetItemsTrait;
use Illuminate\Http\Request;
use Exception;
use App\Product;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    use GetItemsTrait;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Fetch product lists from cache if possible, from $url if not.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $expensive = $this->getItems('https://www.appliancesdelivered.ie/search?sort=price_desc', 'expensive');
            $cheap = $this->getItems('https://www.appliancesdelivered.ie/search', 'cheap');

        } catch (Exception $e) {
            // Return Service Unavailable message in case something goes wrong
            abort(503);
        }

        return view('home', compact('expensive', 'cheap'));
    }


    public function showWishList()
    {
        $user = Auth::user();
        $products = $user->products;
        return view('wishlist.index', compact('products'));
    }

    public function addToWishList(Request $request)
    {
        $user = Auth::user();

        // The csrf_token is part of the array, we must exclude it
        $products = $request->except(['_token']);
        foreach ($products as $name => $price)
        {
            // Only save a product if someone includes it in their wish list
            // if there's a product with the name $name, update its $price
            // if no product exists, create a new one
            $item = Product::updateOrCreate(
                ['name' => $name],
                ['price' => $price]);


            // Get products currently in wish list, include new one in the results and sync
            // this prevents duplicated products
            $productsInWishlist = $user->products()->pluck('id');
            $productsInWishlist[] = $item->id;

            $user->products()->sync($productsInWishlist);
        }

        flash('Your products were added to your wishlist.', 'success');
        return redirect('/home');
    }

    public function removeFromWishList(Request $request)
    {
        $user = Auth::user();

        // The csrf_token is part of the array, we must exclude it
        $selected = $request->except(['_token']);

        // strip all $keys from array, leaves only IDs
        $selected = array_values($selected);

        $user->products()->detach($selected);

        flash('Your products were removed from your wishlist.', 'success');
        return redirect('/wishlist');
    }
}
