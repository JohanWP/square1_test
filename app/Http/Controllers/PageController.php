<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Weidner\Goutte\GoutteFacade;
use App\Http\Traits\GetItemsTrait;

class PageController extends Controller
{
    use GetItemsTrait;

    /**
     * Shows two lists of products, Top 10 most expensive & Top 10 cheapest products
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $expensive = $this->getItems('https://www.appliancesdelivered.ie/search?sort=price_desc', 'expensive');
        $cheap = $this->getItems('https://www.appliancesdelivered.ie/search', 'cheap');

        return view('welcome', compact('expensive', 'cheap'));
    }

}
