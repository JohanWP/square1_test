<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Edit logged user profile
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit()
    {
        $user = Auth::user();
        return view('user.edit', compact('user'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {

        // With more complex validations a Form Request would be used
        $rules = [
            'name'      => 'required|max:100',
            'email'     => 'required|email',
        ];
        $this->validate($request, $rules);

        $user = User::find($id);
        $user->update($request->all());

        return redirect('/home');
    }
}
