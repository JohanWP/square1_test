<?php
namespace App\Http\Traits;

use Illuminate\Support\Collection;
use Weidner\Goutte\GoutteFacade;
use Illuminate\Support\Facades\Cache;

trait GetItemsTrait
{
    /**
     * Returns a collection of products from cache, if $var is not in cache, fetch from $url and store $var in cache
     * @param $url  The URL from where to fetch products
     * @param $var  Item stored (or to be stored) in cache
     * @return mixed
     */
    public function getItems($url, $var)
    {
        // Cache will be stored for this many minutes
        $minutes = 30;

        // Retrieves $var from cache or fetch from $url if not exists
        $products = Cache::remember($var, $minutes, function () use ($url)
        {
            $crawler = GoutteFacade::request('GET', $url);
            return $this->getProductCollection($crawler);
        });

        return $products;
    }

    /**
     * Returns a collection with product name and price as $key => $value
     * @param \Symfony\Component\DomCrawler\Crawler $crawler
     * @return Collection
     */
    private function getProductCollection(\Symfony\Component\DomCrawler\Crawler $crawler)
    {
        $prices = collect();
        $products = collect();

        // Gets the product names
        $crawler->filter('h4')->each(function ($node) use ($products)
        {
            if($products->count() < 10)
            {
                $products->push($node->text());
            }
        });

        // Gets products prices
        $crawler->filter('.section-title')->each(function ($node) use ($prices) {
            if($prices->count() < 10)
            {
                $prices->push($node->text());
            }
        });

        // Combine both product name and price collections, i.e. ['product name' => '$100']
        $combined = $products->combine($prices);
        return $combined;
    }

}