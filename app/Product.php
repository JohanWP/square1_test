<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'name', 'price'
    ];

    /**
     * A product may be in many users lists
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->belongsToMany('App\User')->withTimestamps();
    }

    /**
     * Removes anything not numeric, including symbols like euro and dollar sign from price
     * @param $value
     */
    public function setPriceAttribute($value)
    {
        $this->attributes['price'] = preg_replace("/[^0-9.]/", "", $value);
    }
}
