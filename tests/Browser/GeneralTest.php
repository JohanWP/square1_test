<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;

class GeneralTest extends DuskTestCase
{

    /**
     * Test Landing Page.
     *
     * @return void
     */
    public function testLandingPage()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                ->assertSee('Square1 Test');
        });
    }


    /**
     * Test Login Page.
     *
     * @return void
     */
    public function testLoginPage()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/login')
                ->assertSee('Login');
        });
    }


    /**
     * Test Password reset Page.
     *
     * @return void
     */
    public function testPasswordResetPage()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/password/reset')
                ->assertSee('Send Password Reset Link');
        });

    }
    /**
     * Test home page is only for authorized Users.
     *
     * @return void
     */
    public function testHomePageForUnauthenticatedUsers()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/home')
                ->assertPathIs('/login');
        });
    }

    /**
     * Test 404 Error page.
     *
     * @return void
     */
    public function test404Page()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/jhjajahja')
                ->assertSee('Oops!');
        });
    }

    /**
     * Test send password reset user not exists.
     *
     * @return void
     */
    public function testSendPasswordResetUserNotExists()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/password/reset')
                ->type( 'email', 'notexistingemail@gmail.com')
                ->press('Send Password Reset Link')
                ->assertSee('We can\'t find a user with that e-mail address.');
        });
    }
}
